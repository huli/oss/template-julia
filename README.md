<img src="Logo.png" />

Template.jl
===========

Pkg
---
Basic package workflow:

    ] generate Template #Populate Project.toml
    $ julia --project   #or ] activate .
    ] add ...           #Populate Manifest.toml
    ] instantiate       #download packages
    ] test              #evaluate test/runtests.jl

Essentials
----------
* DataFrames
* Gadfly
* Revise

Emacs
-----
(setq julia-indent-offset 2)

<3
