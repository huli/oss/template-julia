using .Template

# ======================================================================
FUNCS = Dict(
  "--func" => func,
)
PARAMS = Dict(
  "param"  => :param,
)

function func(param)

end

# ======================================================================
function main()
  func  = ARGS[1]
  param = ARGS[2]
  FUNCS[func](PARAMS[param])
end

if abspath(PROGRAM_FILE) == @__FILE__
  main()
end #run
