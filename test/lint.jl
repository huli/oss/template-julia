@testset "lint" begin

  @test Test.detect_ambiguities(Template.Mod) == []

  @test Test.detect_unbound_args(Template.Mod) == []

end #lint
